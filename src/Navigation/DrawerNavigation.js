import React from 'react';
import { Text, SafeAreaView, ImageBackground, StyleSheet, Image, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { NavigationContainer, DrawerActions } from '@react-navigation/native';
import { Drawer } from 'react-native-paper';
// import { Container } from './styles';

const DrawerNavigation = ({navigation}) => {
    const [active, setActive] = React.useState('');

    return (
        <SafeAreaView>
            <ImageBackground 
                    source={require('../../assets/users/backgroundimage.png')}
                    style={{ width: undefined, padding: 16, paddingTop: 48}}
                >
                    <Image source={require('../../assets/users/magic_gladiator.jpg')} style={styles.profile}/>
                    <Text style={styles.name}>Mu Online</Text>

                    <View style={{ flexDirection: 'row'}}>
                        {/* <Text style={styles.followers}>734 Followers</Text> */}
                        {/* <Ionicons name="md-people" size={16} color="rgba(255,255,255, 0.8)" /> */}
                    </View>
            </ImageBackground>
            <Drawer.Section title=" ">
                <Drawer.Item
                    label="Home"
                    active={active === 'first'}
                    icon="home"
                    onPress={() => {
                        setActive('first')
                        navigation.dispatch(DrawerActions.toggleDrawer())
                        navigation.navigate('HomeScreen')
                    }}
                />
                <Drawer.Item
                    label="Spots"
                    active={active === 'second'}
                    icon="nfc-search-variant"
                    onPress={() => {
                        setActive('second')
                        navigation.dispatch(DrawerActions.toggleDrawer())
                        navigation.navigate('Maps')
                    }}
                />
            </Drawer.Section>
        </SafeAreaView>
    );
}

export default DrawerNavigation;

const styles = StyleSheet.create({
    container : {
        flex: 1
    },
    profile: {
        width: 80,
        height: 80,
        borderRadius: 40,
        borderWidth: 3,
        borderColor: '#FFF'
    },
    name: {
        color: '#FFF',
        fontSize: 20,
        fontWeight: '800',
        marginVertical: 8
    },
    followers: {
        color: "rgba(255,255,255, 0.8)",
        fontSize: 13,
        marginRight: 4
    }
})