import { createStore } from 'redux'
import cartItemsReducer from '../redux/CartItem'

const store = createStore(cartItemsReducer)

export default store