import * as React from 'react';
import { Appbar } from 'react-native-paper';
import { StyleSheet, Platform } from 'react-native';

const HeaderComponent = ({title, subtitle, routeBack, onlyBack, navigation}) => {
  const _goBack = () =>  navigation.navigate(routeBack);
  
  const _onlyBack = () =>  navigation.goBack();

  const _handleSearch = () => console.log('Searching');

  const _openDrawer = () => navigation.openDrawer();

  return (
    <Appbar.Header style={styles.headerColor}>
        {
            routeBack != null
            ? <Appbar.BackAction onPress={_goBack} />
            : null
        }
        
        {
            onlyBack != null
            ? <Appbar.BackAction onPress={_onlyBack} />
            : null
        }
      
      <Appbar.Content style={styles.headerTitle} title={title} subtitle={subtitle} />
      {/* <Appbar.Action icon="magnify" onPress={_handleSearch} /> */}
      <Appbar.Action icon="wrap-disabled" onPress={_openDrawer} />
    </Appbar.Header>
  );
};

export default HeaderComponent;


const styles = StyleSheet.create({
  headerTitle: {
    alignItems: 'center',
  }
});

 