import React from 'react';
import { View } from 'react-native';
import { Footer, FooterTab, Button, Text, Icon } from 'native-base';

// import { Container } from './styles';

const FooterComponent = () => {
  return (
    <Footer>
        <FooterTab>
            <Button vertical onPress={() => this.deslogar()}>
            {/* <Badge ><Text>51</Text></Badge> */}
            <Icon type="MaterialCommunityIcons" name="exit-run" style={{ fontSize: 25, color: 'orangered' }}/>
            <Text>Deslogar</Text>
            </Button>

            <Button   vertical onPress={ () => this.props.navigation.navigate('HistoricodepontosScreen', true)}>
            {/* <Badge ><Text>51</Text></Badge> */}
            <Icon style={{color: '#000'}} active name="paper" />
            <Text style={{color: '#000'}}>Histórico de Pontos</Text>
            </Button>
        </FooterTab>
    </Footer>
  );
}

export default FooterComponent;