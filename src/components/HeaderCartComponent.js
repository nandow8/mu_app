import * as React from 'react';
import { Appbar } from 'react-native-paper';
import { StyleSheet, Platform, View, Text } from 'react-native';
import {Ionicons} from '@expo/vector-icons'

import { useSelector } from 'react-redux'

function HeaderCartComponent({title, subtitle, routeBack, onlyBack, navigation}) {
  // const navigation = useNavigation()
  const cartItems = useSelector(state => state)

  const _routeBack = () =>  navigation.navigate(routeBack);
  
  const _onlyBack = () =>  navigation.goBack();

  const _handleSearch = () => console.log('Searching');

  const _goToCart = () => navigation.navigate('CartScreen');

  return (
    <Appbar.Header style={styles.headerColor}>
        {
            routeBack != null
            ? <Appbar.BackAction onPress={_routeBack} />
            : null
        }
        
        {
            onlyBack != null
            ? <Appbar.BackAction onPress={_onlyBack} />
            : null
        }
      
      <Appbar.Content style={styles.headerTitle} title={title} subtitle={subtitle} />
      <View style={styles.itemCountContainer} >
        <Text style={styles.itemCountText} onPress={_goToCart}>{cartItems.length}</Text>
      </View>
      <Ionicons name='ios-cart' size={32} color='#ffffff' onPress={_goToCart} />
    </Appbar.Header>
  );
};

export default HeaderCartComponent;


const styles = StyleSheet.create({
  headerTitle: {
    alignItems: 'center',
  },
  button: {
    marginRight: 10
  },
  itemCountContainer: {
    position: 'absolute',
    height: 30,
    width: 30,
    borderRadius: 15,
    backgroundColor: '#FF7D7D',
    right: 29,
    bottom: 10,
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 2000
  },
  itemCountText: {
    color: 'white',
    fontWeight: 'bold'
  }
});

 