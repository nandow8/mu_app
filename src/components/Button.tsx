import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { RectButton } from 'react-native-gesture-handler';

interface ButtonProps {
    variant: "default" | "primary";
    label: string,
    estilo: string,
    onPress: () => void
}

const Button = ({ variant, label, onPress, estilo}: ButtonProps) => {
    const backgroundColor = variant === "primary" ? "#2CB9B0" : "rgba(12, 13, 52, 0.05)" 
    const color = variant === "primary" ? "white": "#0C0D34"

    return (
        <RectButton 
            style={[styles.container, { backgroundColor }]}
            {...{onPress}}
        >
            <Text style={styles.label}>{ label }</Text>
        </RectButton>
    )
};

export default Button;

const styles = StyleSheet.create({
    container: {
        borderRadius: 25,
        height: 50,
        width: 245,
        justifyContent: "center",
        alignItems: "center"
    },
    label: {
        // fontFamily: 'SFProText-Regular'
        fontSize: 15,
        textAlign: "center"
    },
});