import React from 'react';
import { View, Button, StyleSheet, Text, Image, Dimensions } from 'react-native';
import Animated from 'react-native-reanimated';
import { TouchableOpacity } from 'react-native-gesture-handler';
// import Button from '../../components/Button.tsx'

interface SubslideProps {
    subtitle: string;
    description: string;
    last?: Boolean;
    onPress: () => void
}

const {width, height} = Dimensions.get("window")

const Slide = ({subtitle, description, last, onPress, navigation, monster_id}: SubslideProps) => {
  return (
    <View style={styles.container}>
        <Text style={styles.subtitle}>{subtitle}</Text>
        <Text style={styles.description}>{description}</Text>
        <View style={styles.grid}>
      {/* <TouchableOpacity style={styles.buttonStyle}
        onPress={() => onPress()}
        >
        <Text style={styles.textStyle}>Status</Text>
      </TouchableOpacity> */}

            <Text></Text>
      <TouchableOpacity style={styles.buttonStyle}
			  onPress={() => navigation.navigate('NormalDrop', {monster_id})}
		  >
			 <Text style={styles.textStyle}>Normal Drop</Text>
		  </TouchableOpacity>
          
          <Text></Text>
          
      <TouchableOpacity style={styles.buttonStyle}
        onPress={() => navigation.navigate('ExcellentDrop', {monster_id})}
      >
      <Text style={styles.textStyle}>Excellent Drop</Text>
      </TouchableOpacity>
          <Text></Text>
          
      <TouchableOpacity style={styles.buttonStyle}
        onPress={() => navigation.goBack()}
      >
      <Text style={styles.textStyle}>Back</Text>
      </TouchableOpacity>
        </View>
    </View>
  )
}

export default Slide

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        padding: 44,
        marginBottom: 50
        // width
    },
    subtitle: {
        // fontFamily: "SFProText-Semibold",
        fontSize: 24,
        lineHeight: 30,
        marginBottom: 12,
        color: "#0C0D34",
        textAlign: "center",
    },
    description: {
        // fontFamily: "SFProText-Semibold",
        fontSize: 16,
        lineHeight: 24,
        color: "#0C0D34",
        textAlign: "center",
        marginBottom: 40
    },
    grid: {
        marginRight: '55%',
        marginBottom: 5
    },
    textStyle: {
        fontSize:20,
        color: '#ffffff',
        textAlign: 'center'
      },
      
      buttonStyle: {
        padding:10,
        backgroundColor: '#202646',
        borderRadius:5
      }
})