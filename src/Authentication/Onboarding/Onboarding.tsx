import React, {useRef} from 'react';
import { View, StyleSheet, Dimensions } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';

import Slide, { SLIDE_HEIGHT, BORDER_RADIUS } from './Slide'
import SubSlide from './SubSlide'
import Dot from './Dot'
import Animated, { multiply, divide } from 'react-native-reanimated';
import {useValue, onScrollEvent, interpolateColor, useScrollHandler} from 'react-native-redash'
import { Text } from 'native-base';

const {width, height} = Dimensions.get("window")
const slides = [
  {title: "Cursed Wizard", subtitle: "sub1", description: "mago negro que nao gosta de magia branca.", color: "#9993", picture: require('../../../assets/monstros/TesteGif4.gif')},
  {title: "Cursed Wizard", subtitle: "sub2", description: "desc2", color: "#9993", picture: require('../../../assets/monstros/TesteGif5.gif')},
  // {title: "Status", subtitle: "sub3", description: "desc3", color: "#9993", picture: require('../../../assets/monstros/TesteMago.gif')},
  // {title: "Funky", subtitle: "sub4", description: "desc4", color: "#FFDDDD"}
]

const Onboarding = ({route, navigation}) => {
  const monster_id = route.params.monster.monster_id;
  const title = route.params.monster.name;
  const picture = route.params.monster.monster_imagem

  const scroll = useRef<Animated.ScrollView>(null);
  const { scrollHandler, x } = useScrollHandler()
  const backgroundColor = interpolateColor(x, {
    inputRange: slides.map((_, i) => i * width),
    outputRange: slides.map(slide => slide.color)
  })
  return (
    <View style={styles.container}>
      <Animated.View style={[styles.slider, {backgroundColor}]}>
        <Animated.ScrollView 
          ref={scroll}
          horizontal 
          snapToInterval={width} 
          descelerationRate="fast" 
          showsHorizontalScrollIndicator={false} 
          bounces={false}
          {...scrollHandler}
        >
          
              <Slide right={false} {...{ title, picture }} />
          
          
        </Animated.ScrollView>
      </Animated.View>
      <View style={styles.footer}>
        <Animated.View 
          style={{...StyleSheet.absoluteFillObject, backgroundColor }} 
        />
        <View style={styles.footerContent}>

          {/* <View style={styles.pagination}>
            {slides.map((_, index) => (
              <Dot key={index} currentIndex={divide(x, width)} {...{index, x}} />
            ))}
          </View> */}

          <Animated.View style={{
            // width: width * slides.length, 
            flex: 1, 
            flexDirection: "row",
            width: width * slides.length,
            transform: [{ translateX: multiply(x, -1)}]
          }}>
            
           
              <SubSlide 
                navigation={navigation}
                monster_id={monster_id}
                onPress={() => {
                  if (scroll.current) {
                    scroll.current
                    .getNode()
                    .scrollTo({ x: width * 1, animated: true})
                  }
                }}
                last={true}
                
              />
          </Animated.View>
        </View>
      </View>
    </View>
  )
}

export default Onboarding

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  },
  slider: {
    height: SLIDE_HEIGHT,
    // backgroundColor: "cyan",
    borderBottomRightRadius: BORDER_RADIUS
  },
  footer: {
    flex: 1
  },
  footerContent: {
    flex: 1,
    // flexDirection: "row",
    backgroundColor: "white",
    borderTopLeftRadius: BORDER_RADIUS
  },
  pagination: {
    ...StyleSheet.absoluteFillObject,
    height: BORDER_RADIUS,
    // backgroundColor: 'rgba(100,200,300,0.5)'
    // width,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  }
})