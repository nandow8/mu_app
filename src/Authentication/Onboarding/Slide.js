import React from 'react';
import { View, StyleSheet, Text, Dimensions, Image } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import UrlHelper from '../../Constants/UrlHelper';

const {width, height} = Dimensions.get("window")
export const SLIDE_HEIGHT = 0.61 * height
export const BORDER_RADIUS = 75

interface SlideProps {
    title: string;
    right?: Boolean;
    picture: number,
}

const Slide = ({title, right, picture}: SlideProps) => {
  const transform = [
    {translateY: (SLIDE_HEIGHT - 100) / 2},
    {translateX: right ? width / 2 - 50 : -width / 2 + 20},
    { rotate: right ? "-90deg" : "90deg"}
  ]
  return (
    <View style={styles.container}>
      
      <View style={styles.underlay}>
        <Image 
            source={{
              uri: UrlHelper.apiImageUrl(picture),
            }}
            style={styles.picture}
        /> 
      </View>

      <View style={[styles.titleContainer, {transform}]}>
        <Text style={styles.title}>{title}</Text>
      </View>
    </View>
  )
}

export default Slide

const styles = StyleSheet.create({
  container: {
    width,
    // overflow: 'hidden',
  },
  title: {
    fontSize: 45,
    lineHeight: 80,
    color: "white",
    textAlign: "center",
    height: 100
  },
  titleContainer: {
    // backgroundColor: "red",
    height: 100,
    justifyContent: "center"
  },
  underlay: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
  },
  picture: {
    ...StyleSheet.absoluteFillObject,
    // top: SLIDE_HEIGHT - SLIDE_HEIGHT * 0.61,
    width: undefined,
    height: undefined,
    borderBottomRightRadius: BORDER_RADIUS,
  },
})