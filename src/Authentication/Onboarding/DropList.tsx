import * as React from 'react';
import { Text, View, StyleSheet } from 'react-native';

interface componentNameProps {
    nome: string;
    descricao: string;
}

const componentName = ({nome, descricao}: componentNameProps) => {
  return (
    <View style={styles.container}>
      <Text>componentName</Text>
    </View>
  );
};

export default componentName;

const styles = StyleSheet.create({
  container: {}
});
