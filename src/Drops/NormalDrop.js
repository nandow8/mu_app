import * as React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { Card, Title, Paragraph, ActivityIndicator, Colors  } from 'react-native-paper';
import { ScrollView } from 'react-native-gesture-handler';
import HeaderComponent from '../components/HeaderComponent';
import UrlHelper from '../Constants/UrlHelper';
import axios from 'axios'

class NormalDrop extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
          itens: [],
          loading: true
        }

        // NetInfo.fetch().then(state => {
            // console.log('Connection type', state.type);
        //     this.setState({ isConnected: state.isConnected });
        // });

    }
    UNSAFE_componentWillMount() {
        this.getNormalItens(this.props.route.params.monster_id)
    }

    UNSAFE_componentWillReceiveProps(old, next) {
        this.getNormalItens(old.route.params.monster_id)
    }

    getNormalItens = async (monster_id) => {
        await axios.get(UrlHelper.apiUrl(`finditensbymonsterid/${monster_id}/1`)).then(res => { // /1 itens normais /2 itens ex /3 ancients
            this.setState({
                itens: res.data,
                loading: false
            })
        })
    }

    itensCategories= (param) =>{
        if (param == 'Set') {
            return 'Set'
        }

        if (param == 'phyWeapon') {
            return 'Physical Weapons'
        }
        
        if (param == 'wizWeapon') {
            return 'Wizardry Weapons'
        }
        
        if (param == 'shields') {
            return 'Shields'
        }
        
        if (param == 'skill_itens') {
            return 'Skill Itens'
        }
        
        if (param == 'craf_itens') {
            return 'Craf Itens'
        }
        
        if (param == 'acessories') {
            return 'Accessories'
        }
        
        if (param == 'other_itens') {
            return 'Others'
        }
    }

    render() {
        return (
            <>
                <HeaderComponent title="Normal Drop" onlyBack="Spots" navigation={this.props.navigation}/>
                <ScrollView style={styles.container}>
                    {
                        this.state.loading
                        ?   <ActivityIndicator animating={true} color={Colors.red800} />
                        :   this.state.itens && Object.values(this.state.itens).map((data, index) => (
                                <Card key={index} style={styles.card}>
                                    <Card.Content>
                                    <Title>{this.itensCategories(data[0].equip_type)}</Title>
                                    <Paragraph>
                                        {
                                            Object.values(data).map((dt, i) => (
                                                <Text key={i}> {dt.name},</Text>
                                            ))
                                        }
                                    </Paragraph>
                                    </Card.Content>
                                </Card>
                            ))
                    }
                </ScrollView>
            </>
      );
    }
};

export default NormalDrop;

const styles = StyleSheet.create({
    container: {
        // margin: 5
    },
    card: {
        margin: 5,
        
    },

    textStyle: {
        fontSize:15,
        color: '#ffffff',
        textAlign: 'center'
      },
      
      palavrasBotoes: {
        padding: 5,
        backgroundColor: '#202646',
        color: '#ffffff',
        borderRadius:5
      }
});
