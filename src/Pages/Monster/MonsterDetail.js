import React, { Component } from 'react';
import { Text, View, Image } from 'react-native';
import UrlHelper from '../../Constants/UrlHelper'
import styles from './Styles/CartItemStyles';
import { TouchableHighlight, TouchableOpacity } from 'react-native-gesture-handler';

const MonsterDetail = ({navigation, props}) => {

  const _toMonsterDetail = () =>  navigation.navigate('Onboarding', {monster: props});

  return (
        <View style={ styles.ItemContainer }>
            <Text></Text>
            <Text></Text>
            
            <View style={styles.BodyContainer}>
              <Image
                source={{
                  uri: UrlHelper.apiImageUrl(props.monster_imagem),
                }}
                style={styles.ItemImage}
              />
              <TouchableOpacity onPress={_toMonsterDetail}>
                <View style={styles.ImageTextContainer}>
              <Text style={styles.monsterName}>{props.name} </Text>
                   <Text style={styles.ItemTitle}>Level	</Text>
                   <Text style={styles.ItemTitle}>Physical Strength	</Text>
                   <Text style={styles.ItemTitle}>Min Striking Power</Text>
                   <Text style={styles.ItemTitle}>Max Striking Power</Text>
                   <Text style={styles.ItemTitle}>Def Power</Text>
                   <Text style={styles.ItemTitle}>Def Rate</Text>
                </View>
                </TouchableOpacity>

                
                  <View style={styles.ImageTextContainer}>
                      <Text style={styles.ItemPrice}></Text>
                      <Text style={styles.ItemPrice}>{props.level}</Text>
                      <Text style={styles.ItemPrice}>{props.physical_strength}</Text>
                      <Text style={styles.ItemPrice}>{props.min_striking_power}</Text>
                      <Text style={styles.ItemPrice}>{props.max_striking_power}</Text>
                      <Text style={styles.ItemPrice}>{props.def_power}</Text>
                      <Text style={styles.ItemPrice}>{props.def_rate}</Text>
                  </View>
                
                  
            </View>
        </View>
  );
}

export default MonsterDetail
