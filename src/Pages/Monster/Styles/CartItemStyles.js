import { StyleSheet, Platform } from 'react-native'
// import { Colors, Metrics, Fonts } from '../../Themes/'

export default StyleSheet.create({
    ItemContainer: {
        // backgroundColor: 'blue',
        width: '100%',
        height: 160
    },

    BodyContainer: {
        flex: 1,
        flexDirection:  'row',
        justifyContent: 'space-around',
        marginLeft: 20,
        width: '90%'
    },

    ItemImage: {
        width: '40%',
        height: '100%',
        borderRadius: 20
    },

    ImageTextContainer: {
        marginRight: 1,
        flexDirection: 'column',
        justifyContent: 'space-evenly',
    },

    ItemTitle: {
        color: 'black',
        fontSize: 15,
        fontWeight: 'bold'
    },

    monsterName: {
        color: 'orangered',
        fontSize: 17,
        fontWeight: 'bold'
    },
   
    ItemPrice: {
        color: 'blue',
        fontSize: 15,
        fontWeight: 'bold'
    },

    ItemContentContainer: {
        position: 'absolute',
        right: 0,
        width: 70,
        height: '100%',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },

    ItemCountUp: {
        fontSize: Platform.OS === 'ios' ? 30:50
    },
    
    ItemCountText: {
        fontSize: 15,
        color: 'black',
        marginRight: 12
    },
    
    ItemCountDown: {
        fontSize: Platform.OS === 'ios' ? 30:50,
        color: 'gray'
    }
})
