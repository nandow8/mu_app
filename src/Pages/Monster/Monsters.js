import React, { Component } from 'react';
import { View, Text, ScrollView } from 'react-native';
import { Content } from 'native-base';
import HeaderComponent from '../../components/HeaderComponent';
import styles from './Styles/CartStyle'

import MonsterDetail from './MonsterDetail';

const Monsters = ({navigation, route}) => {
    const {params} = route
    return (
        <View>
            <HeaderComponent title="Monsters in Spot" onlyBack="Spots" navigation={navigation}/>
            <ScrollView style={ styles.ParentView }>
                <Content>
                    {
                        params.monsters && params.monsters.map((data, index) =>
                            <MonsterDetail key={index} props={data}  navigation={navigation} />
                        )
                    }
                    <Text></Text>
                    <Text></Text>
                    <Text></Text>
                    <Text></Text>
                    <Text></Text>
                    <Text></Text>
                </Content>
            </ScrollView>
        </View>
    );
}

export default Monsters
