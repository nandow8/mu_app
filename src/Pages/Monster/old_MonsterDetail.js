import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, Image } from 'react-native';
import { Table, TableWrapper, Row, Rows, Col } from 'react-native-table-component';
import HeaderComponent from '../../components/HeaderComponent';
 
export default class MonsterDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tableHead: ['Dark Wizard', 'Status'],
      tableData: [
        [ <Image 
            source={require('../../../assets/monstros/TesteGif4.gif')}
            style={styles.picture}
        /> , '3'],
       
      ]
    }
  }
 
  render() {
    const state = this.state;
    return (
        <View style={styles.containertop}>
            <HeaderComponent title="Monsters in Spot" routeBack="HomeScreen" navigation={this.props.navigation}/>
            <ScrollView>
                <View style={styles.container}>
                    <Table borderStyle={{borderWidth: 1}}>
                    <Row data={state.tableHead} flexArr={[3, 3]} style={styles.head} textStyle={styles.text}/>
                    <TableWrapper style={styles.wrapper}>
                        <Rows data={state.tableData} flexArr={[3, 3]} style={styles.row} textStyle={styles.text}/>
                    </TableWrapper>
                    </Table>
                </View>
            </ScrollView>
        </View>
    )
  }
}

const styles = StyleSheet.create({
    container: {backgroundColor: 'gray' },
    container: { flex: 1, padding: 16, paddingTop: 20, backgroundColor: 'lightgray' },
    head: {  height: 40,  backgroundColor: '#f1f8ff'  },
    wrapper: { flexDirection: 'row' },
    title: { flex: 1, backgroundColor: '#f6f8fa' },
    row: {  height: 228  },
    text: { textAlign: 'center' },
    picture: {
        width: '100%',
        height: '100%',
    }
  });