import React, { Component } from 'react';
import { View, Text, ScrollView, StyleSheet, TextInput } from 'react-native';
import HeaderComponent from '../../components/HeaderComponent';
import { Card, Avatar } from 'react-native-paper';

export default class WcoinList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      texto: ''
    };
  }

  render() {
    return (
      <>
        <HeaderComponent title="Wcoin" routeBack="HomeScreen" navigation={this.props.navigation} />
        <ScrollView>
            <Card style={styles.card} onPress={() => this.props.navigation.navigate('SpotsDetailsList', 1)}>
                    <Card.Title
                        title="500"
                        subtitle={`R$ 14,90`}
                        left={(props) => <Avatar.Image size={49} source={require('../../../assets/wcoin/500WCoins.png')} />}
                        // right={(props) => <IconButton {...props} icon="folder" onPress={() => {}} />}
                    />
            </Card>
            <Card style={styles.card} onPress={() => this.props.navigation.navigate('SpotsDetailsList', 1)}>
                    <Card.Title
                        title="peititle"
                        subtitle={`Levelsubtitle`}
                        left={(props) => <Avatar.Image size={49} source={require('../../../assets/wcoin/1000WCoins.png')} />}
                        // right={(props) => <IconButton {...props} icon="folder" onPress={() => {}} />}
                    />
            </Card>
            <Card style={styles.card} onPress={() => this.props.navigation.navigate('SpotsDetailsList', 1)}>
                    <Card.Title
                        title="peititle"
                        subtitle={`Levelsubtitle`}
                        left={(props) => <Avatar.Image size={49} source={require('../../../assets/wcoin/2000WCoins.png')} />}
                        // right={(props) => <IconButton {...props} icon="folder" onPress={() => {}} />}
                    />
            </Card>
            <Card style={styles.card} onPress={() => this.props.navigation.navigate('SpotsDetailsList', 1)}>
                    <Card.Title
                        title="peititle"
                        subtitle={`Levelsubtitle`}
                        left={(props) => <Avatar.Image size={49} source={require('../../../assets/wcoin/3000WCoins.png')} />}
                        // right={(props) => <IconButton {...props} icon="folder" onPress={() => {}} />}
                    />
            </Card>
          
            <TextInput  />
        </ScrollView>
      </>
    );
  }
}

const styles = StyleSheet.create({
    card: {
        margin: 8,
    },
    container: {
        backgroundColor: 'lightgray'
    }
});