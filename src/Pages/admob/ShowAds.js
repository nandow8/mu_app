import React, {useEffect} from 'react';
import { Button } from 'react-native';
import {
    AdMobBanner,
    AdMobInterstitial,
    setTestDeviceIDAsync,
  } from 'expo-ads-admob';

// import { Container } from './styles';

export default function ShowAds() {
    useEffect(() => {
        async function loadAd() {
            AdMobInterstitial.setAdUnitID('ca-app-pub-9116496068256980/4424423684');
            intertitialID()
        }
        loadAd()
    }, [])

    async function intertitialID() {
        await AdMobInterstitial.requestAdAsync({ servePersonalizedAds: true })
        await AdMobInterstitial.showAdAsync()
    }

    return (
        <>
            <Button title="Show Ads" onPress={intertitialID} />
            <AdMobBanner
                bannerSize="fullBanner"
                adUnitID="ca-app-pub-9116496068256980/3531086361"
                setTestDeviceIDAsync
                servePersonalizedAds
                onDidFailToReceiveAdWithError={(err) => console.log(err)} 
            />
        </>
    )
}
