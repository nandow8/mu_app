import React, { Component } from 'react';
import { View, Text, Button } from 'react-native';

import { useSelector, useDispatch } from 'react-redux'
import OutroList from './OutroList'

export default function CourseList() {
    const qtd = 1
    const courses = useSelector(state => state.data.slice(0, qtd), [qtd])
    const dispatch = useDispatch()

    function addCourse() {
        dispatch({ type: 'ADD_COURSE', title: 'oooo' })
    }

    return (
        <View>
            {
                courses.map((course, index) => <Text key={index}>{course}</Text>)
            }
            <Button title='ADd curso' onPress={addCourse} />
            
            <OutroList />
        </View>
    )
}