import React from 'react'
import {useSelector} from 'react-redux'
import { View, Text } from 'react-native'

export default function OutroList() {
    const courses = useSelector(state => state.data)
    return (
        <View>
            { courses.map((course, index) => <Text key={index}>{course}</Text>)}
            
        </View>
    )
}