import React from 'react';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import HeaderComponent from '../../components/HeaderComponent';
import { ScrollView } from 'react-native-gesture-handler';
import { Card, ActivityIndicator, Colors, Avatar } from 'react-native-paper';
import NetInfo from '@react-native-community/netinfo';
import UrlHelper from "../../Constants/UrlHelper";

// import { Container } from './styles';
import * as SQLite from 'expo-sqlite'
const db = SQLite.openDatabase('db.testDb')
import axios from 'axios'


class Maps extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
          data: null,
          maps: [],
          isOnline: false,
          loading: true
        }

        NetInfo.fetch().then(state => {
            // console.log('Connection type', state.type);
            this.setState({ isConnected: state.isConnected });
        });

        this.getMaps()
    }

    fetchData = () => {
        db.transaction(tx => {
            tx.executeSql('SELECT * FROM spots', null, 
                (txObj, { rows: { _array } }) => this.setState({ maps: _array }) 
            )
        }) 
        this.setState({
            loading: false
        })
    }

    getMaps = async () => {
        if (this.state.isOnline !== true) {
            await axios.get(UrlHelper.apiUrl('maps')).then(res => {
                this.setState({
                    maps: res.data,
                    loading: false
                })
                this.newItem(res.data)
            })
        } else {
            this.fetchData()
        }
    }

    newItem = (array) => {
        db.transaction(tx => {
            tx.executeSql(
                'DROP TABLE spots'
            )
            tx.executeSql(
                'CREATE TABLE IF NOT EXISTS spots (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, level_monsters TEXT)'
            )
            array.forEach(element => {
                tx.executeSql('INSERT INTO spots (id, name, level_monsters) values (?, ?, ?)', [element.id, element.name, element.level_monsters],
                  (txObj, resultSet) => this.setState({ maps: this.state.maps.concat(
                      { id: resultSet.insertId, name: resultSet.name, level_monsters: resultSet.level_monsters }) }),
                  (txObj, error) => console.log('Error', error))
            })
        });
    }

    render () {
        return (
            <>
                <HeaderComponent title="Spots" routeBack="HomeScreen" navigation={this.props.navigation}/>
                {
                    this.state.loading
                    ?   <ActivityIndicator size="large" animating={this.state.loading} color={Colors.red800} />
                    :   <ScrollView style={styles.container}>
                            {
                                this.state.maps && this.state.maps.map((data, index) =>
                                (
                                        data.name == undefined
                                        ? null
                                        :   <Card key={index} style={styles.card} onPress={() => this.props.navigation.navigate('SpotsDetailsList', {map_id: data.id, map_name: data.name})}>
                                                <Card.Title
                                                    title={data.name}
                                                    subtitle={`Level ${data.level_monsters}`}
                                                    left={(props) => <Avatar.Image size={49} source={require('../../../assets/icon.png')} />}
                                                    // right={(props) => <IconButton {...props} icon="folder" onPress={() => {}} />}
                                                />
                                            </Card>
                                    
                                ))
                            }
                        </ScrollView>
                }
            </>
        );
    }
}

export default Maps;


const styles = StyleSheet.create({
    card: {
        margin: 8,
    },
    container: {
        backgroundColor: 'lightgray'
    }
});