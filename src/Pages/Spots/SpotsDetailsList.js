import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import HeaderComponent from '../../components/HeaderComponent';
import { ScrollView } from 'react-native-gesture-handler';
import { Card, ActivityIndicator, Colors, Paragraph } from 'react-native-paper';
import NetInfo from '@react-native-community/netinfo';
import UrlHelper from "../../Constants/UrlHelper";

// import { Container } from './styles';
import * as SQLite from 'expo-sqlite'
const db = SQLite.openDatabase('db.testDb')
import axios from 'axios'
import { pinchEnd } from 'react-native-redash';
// import { Container } from './styles';

class SpotsDetailsList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            map_id: '',
            map_name: '',
            isConnected: false,
            loading: true,
            spots: []
        }

        NetInfo.fetch().then(state => {
            // console.log('Connection type', state.type);
            this.setState({ isConnected: state.isConnected });
        });
    }

    UNSAFE_componentWillMount() {
        const { map_id, map_name } = this.props.route.params

        this.setState({
            map_id: map_id,
            map_name: map_name  
        })

        this.spotsList(map_id)
    }

    UNSAFE_componentWillReceiveProps(old, next) {
        this.setState({
            map_id: old.route.params.map_id,
            map_name: old.route.params.map_name  
        })

        this.spotsList(old.route.params.map_id)
    }

    spotsList = async (map_id) => {
        // console.log(map_id, 'SSS');
        await axios.get(UrlHelper.apiUrl(`spots/${map_id}`)).then(res => {
            this.setState({
                spots: res.data,
            })
            // console.log(res.data);
        })

        this.setState({
            loading: false
        })
    }

    render () {
        return (
            <>
                <HeaderComponent title={this.state.map_name} routeBack="Maps" navigation={this.props.navigation}/>
                <ScrollView style={styles.container}> 
                    {
                        this.state.loading
                        ?   <ActivityIndicator animating={true} color={Colors.red800} />
                        :   Object.values(this.state.spots).map((data, index) => 
                            // Object.values(data).map((hist, i) => 
                                <Card key={index} style={styles.card} onPress={() => this.props.navigation.navigate('Monsters', {monsters: data})}>
                                    <Card.Cover style={styles.image} source={{
                                                            uri: UrlHelper.apiImageUrl(data[0].imagem),
                                                        }}  />
                                    <Card.Content>
                                        <Card.Title
                                            title={`X: ${data[0].coord_x} / Y: ${data[0].coord_y}`}
                                            subtitle={data[0].monsters_in_spot}
                                            // left={(props) => <Paragraph>monsters: 5</Paragraph>}
                                            right={(props) => <Paragraph>Monsters: {data[0].qtd}</Paragraph>}
                                        />
                                        
                                    </Card.Content>
                                </Card>
                            // )
                            )
                    }

                    {
                        this.state.spots.length < 1
                        ? <Text style={styles.naoEncontrado}>Spot nao cadastrado no sistema</Text>
                        : null
                    }
                </ScrollView>
            </>
        );
    }
}

export default SpotsDetailsList;

const styles = StyleSheet.create({
    card: {
        margin: 8,
    },
    container: {
        backgroundColor: 'lightgray'
    },
    image: {
        marginTop: 8,
        marginLeft: 5,
        marginRight: 5,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        borderBottomRightRadius: 20,
        borderBottomLeftRadius: 20,
    },
    naoEncontrado: {
        textAlign: 'center',
        marginTop: '50%',
        fontSize: 16
    }
});