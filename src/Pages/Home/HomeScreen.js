import React from 'react';
import { View, StyleSheet, Image } from 'react-native';
import HeaderComponent from '../../components/HeaderComponent'
import { Card, Button } from 'react-native-paper';
import { ScrollView } from 'react-native-gesture-handler';

const HomeScreen = ({navigation}) => {
  return (
      <>
        <HeaderComponent title="Welcome" navigation={navigation}/>
        <ScrollView style={styles.container}>
            <Card style={styles.card} onPress={() => navigation.navigate('Maps')}>
                <Image 
                    style={{ width: '100%', height: 180}}
                    source={require('../../../assets/spots/mumap.jpg')} 
                />
                <Card.Actions>
                    <Button>Spots</Button>
                </Card.Actions>
            </Card>
            
            <Card style={styles.card} onPress={() => navigation.navigate('Wcoin')}>
                <Image 
                    style={{ width: '100%', height: 180}}
                    source={require('../../../assets/wcoin/1000WCoins.png')} 
                />
                <Card.Actions>
                    <Button>Wcoin</Button>
                </Card.Actions>
            </Card>
            
        </ScrollView>
      </>
  )
}

export default HomeScreen;


const styles = StyleSheet.create({
    card: {
        margin: 8,
    },
    container: {
        backgroundColor: 'lightgray'
    }
});