import Constants from 'expo-constants';

function getMode(){
    try {
        let channel = Constants.manifest.releaseChannel;
        if(channel === 'develop' || channel === undefined){
            return 1;
        }

        return 0;
    }
    catch{
        return 1;
    }
}

const UrlHelper = {
    developerMode: getMode(),
    apiUrl: (url) => {
        if(UrlHelper.developerMode) {
            return 'http://localhost:8000/api/'+url;
        }
        else{
            return 'http://www.pei.kinghost.net/nandao/mu_api/api/'+url;
        }
    },
    apiImageUrl: (imagemPath) => {
        if(UrlHelper.developerMode) {
            return 'http://localhost:8000/'+imagemPath;
        }
        else{
            return 'http://www.pei.kinghost.net/nandao/mu_api/public/'+imagemPath;
        }
    },
    
}

export default UrlHelper;