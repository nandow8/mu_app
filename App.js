import 'react-native-gesture-handler';
import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';

import Onboarding from './src/Authentication/Onboarding/Onboarding'
import NormalDrop from './src/Drops/NormalDrop'
import HomeScreen from './src/Pages/Home/HomeScreen';
import Maps from './src/Pages/Spots/Maps';
import HeaderComponent from './src/components/HeaderComponent';
import SpotsDetails from './src/Pages/Spots/SpotsDetails';
import SpotsDetailsList from './src/Pages/Spots/SpotsDetailsList';
import MonsterDetail from './src/Pages/Monster/MonsterDetail'
import Monsters from './src/Pages/Monster/Monsters';
import { Text } from 'native-base';
import DrawerNavigation from './src/Navigation/DrawerNavigation';
import { SafeAreaView, StyleSheet } from 'react-native';
import GlobalStyles from './src/Constants/GlobalStyles';
import ExcellentDrop from './src/Drops/ExcellentDrop';
import MonsterProfile from './src/Pages/Monster/MonsterProfile';
import ShowAds from './src/Pages/admob/ShowAds';
import WcoinList from './src/Pages/Wcoin/WcoinList';

import { Provider } from 'react-redux'
import store from './src/store'
import CourseList from './src/Pages/courses/CourseList';
import Wcoin from './src/Pages/Wcoin/Wcoin';
import CartScreen from './src/Pages/Cart/CartScreen';

const AuthenticationStack = createStackNavigator()

const AuthenticationNavigator = () => {
  return (
    <AuthenticationStack.Navigator headerMode="none">      
      <AuthenticationStack.Screen name="HomeScreen" component={HomeScreen} />
      <AuthenticationStack.Screen name="NormalDrop" component={NormalDrop} />
      <AuthenticationStack.Screen name="Onboarding" component={Onboarding} />
      
      <AuthenticationStack.Screen name="Maps" component={Maps} />
      <AuthenticationStack.Screen name="SpotsDetailsList" component={SpotsDetailsList} />
      <AuthenticationStack.Screen name="SpotsDetails" component={SpotsDetails} />
      
      <AuthenticationStack.Screen name="Monsters" component={Monsters} />
      <AuthenticationStack.Screen name="MonsterDetail" component={MonsterDetail} />
      
      
      
    </AuthenticationStack.Navigator>
  )
}

const Drawer = createDrawerNavigator();
const DrawerNavigator = () => {
  return (
    <Drawer.Navigator initialRouteName="HomeScreen" drawerContent={props => <DrawerNavigation {...props}/>}>
      <Drawer.Screen name="HomeScreen" component={HomeScreen} />
      <Drawer.Screen name="NormalDrop" component={NormalDrop} />
      <Drawer.Screen name="ExcellentDrop" component={ExcellentDrop} />
      
      <Drawer.Screen name="Onboarding" component={Onboarding} />
      
      <Drawer.Screen name="Maps" component={Maps} />
      <Drawer.Screen name="SpotsDetailsList" component={SpotsDetailsList} />
      <Drawer.Screen name="SpotsDetails" component={SpotsDetails} />
      
      <Drawer.Screen name="Monsters" component={Monsters} />
      <Drawer.Screen name="MonsterDetail" component={MonsterDetail} />
      
      <Drawer.Screen name="WcoinList" component={WcoinList} />
      
      <Drawer.Screen name="CourseList" component={CourseList} />
      <Drawer.Screen name="Wcoin" component={Wcoin} />
      <Drawer.Screen name="CartScreen" component={CartScreen} />
      

      <Drawer.Screen name="HeaderComponent" component={HeaderComponent} />
    </Drawer.Navigator>
  )
}

export default function App() {
  return (
    <Provider store={store}>
      <SafeAreaView style={GlobalStyles.droidSafeArea}>
      <NavigationContainer>
              
        <DrawerNavigator />
        
      </NavigationContainer>

      {/* <ShowAds /> */}

      </SafeAreaView>
    </Provider>
  );
}
